let displayDesapear = 0;

let box = document.getElementsByClassName('box')[0];

function validation() {
  if (displayDesapear) {
    display.value = '';
    displayDesapear = 0;
  }
}

function doOperation(f) {
  switch (f) {
    case 'div':
      return (+firstNumber) / (+secondNumber);

    case 'mult':
      return (+firstNumber) * (+secondNumber);

    case 'sub':
      return (+firstNumber) - (+secondNumber);

    case 'add':
      return (+firstNumber) + (+secondNumber);

  }

};


function operOnClick(butt, func) {

  butt.onclick = (() => {
    displayDesapear = 1;
    if (firstNumber) {
      secondNumber = display.value;
      display.value = doOperation(currentFunction);
      firstNumber = display.value;
      currentFunction = func;
    } else {
      firstNumber = display.value;
      currentFunction = func;
    }
  });
};

let grayButtons = document.getElementsByClassName('button gray');

let blackButtons = document.querySelectorAll('.black');

let pinkButtons = document.getElementsByClassName('button pink');

let display = document.getElementsByClassName('input-display')[0];

let equal = document.getElementsByClassName('button orange')[0];

let mLatter = document.getElementsByClassName('memory')[0];

let buttonDiv = pinkButtons[0];
let buttonMult = pinkButtons[1];
let buttonSub = pinkButtons[2];
let buttonAdd = pinkButtons[3];

let memory = grayButtons[0];
let memorySub = grayButtons[1];
let memoryAdd = grayButtons[2];

let firstNumber;
let secondNumber;
let currentFunction;

let memoryNumber;
let memoryCount = 0;


blackButtons.forEach(element => {
  if(element.value === '0'){
    element.onclick = (() => {
        if (display.value === '0') {
          validation()
          display.value = 0
        } else {
          display.value += 0
        }
      });
  }else if(element.value === '.'){
    element.onclick = (() => {
      validation()
      display.value += '.'
    });
  }else if(element.value === 'C'){
    element.onclick = (() => {
      display.value = '';
      firstNumber = 0;
      secondNumber = 0;
      currentFunction = ''
    
    });
  }else{
  element.onclick = (() => {
    validation()
    display.value += +element.value;
  });
}
});

operOnClick(buttonDiv, 'div');
operOnClick(buttonMult, 'mult');
operOnClick(buttonSub, 'sub');
operOnClick(buttonAdd, 'add');

equal.onclick = (() => {
  secondNumber = display.value;
  display.value = doOperation(currentFunction);
  firstNumber = display.value;

});

memoryAdd.onclick = (() => {
  memoryNumber = display.value;
  display.value = ''
  mLatter.style.opacity = 1;

});

memorySub.onclick = (() => {
  memoryNumber = '';
  mLatter.style.opacity = 0;
});

memory.onclick = (() => {
  if (!memoryCount) {
    display.value = memoryNumber;
    memoryCount++
  } else {
    memoryCount = 0;
    memoryNumber = ''
    mLatter.style.opacity = 0;
  }
});
